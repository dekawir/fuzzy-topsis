<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_bantuan', function (Blueprint $table) {
            $table->id();
            $table->string('nama_kelompok',50);
            $table->string('alamat',100);
            $table->string('kecamatan',50);
            $table->string('nama_ketua_kelompok',50);
            $table->date('tanggal_pengajuan');

            $table->string('jumlah_anggota',5);
            $table->string('riwayat_bantuan',5);
            $table->string('luas_lahan',5);
            $table->string('keaktifan_kelompok',5);
            $table->string('lokasi_kebun',5);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_bantuan');
    }
};
