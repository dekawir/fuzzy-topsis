<?php

namespace Database\Seeders;

use App\Models\Kriteria;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama_kriteria'=>'Jumlah Anggota',
                'cost_benefit'=>'Benefit',
                'bobot'=>'1',
            ],
            [
                'nama_kriteria'=>'Riwayat Bantuan Pemerintah',
                'cost_benefit'=>'Cost',
                'bobot'=>'1',
            ],
            [
                'nama_kriteria'=>'Luas Lahan',
                'cost_benefit'=>'Benefit',
                'bobot'=>'1',
            ],
            [
                'nama_kriteria'=>'Keaktifan Kelompok',
                'cost_benefit'=>'Benefit',
                'bobot'=>'0.75',
            ],
            [
                'nama_kriteria'=>'Lokasi Kebun Dekat Dengan Fasilitas Transportasi',
                'cost_benefit'=>'Benefit',
                'bobot'=>'0.50',
            ],
        ];

        foreach($data as $data){
            Kriteria::insert($data);
        }
    }
}
