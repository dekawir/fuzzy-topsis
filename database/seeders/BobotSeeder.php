<?php

namespace Database\Seeders;

use App\Models\Bobot;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BobotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id_kriteria'=>'1',
                'range'=>'<=10',
                'nilai'=>'0',
                'nama_himpunan'=>'Rendah',
            ],
            [
                'id_kriteria'=>'1',
                'range'=>'11-15',
                'nilai'=>'0.25',
                'nama_himpunan'=>'Kurang',
            ],
            [
                'id_kriteria'=>'1',
                'range'=>'16-20',
                'nilai'=>'0.50',
                'nama_himpunan'=>'Cukup Baik',
            ],
            [
                'id_kriteria'=>'1',
                'range'=>'21-25',
                'nilai'=>'0.75',
                'nama_himpunan'=>'Baik',
            ],
            [
                'id_kriteria'=>'1',
                'range'=>'>26',
                'nilai'=>'0.1',
                'nama_himpunan'=>'Sangat Baik',
            ],
            // ---------------
            [
                'id_kriteria'=>'2',
                'range'=>'>=2',
                'nilai'=>'0',
                'nama_himpunan'=>'Rendah',
            ],
            [
                'id_kriteria'=>'2',
                'range'=>'1',
                'nilai'=>'0.25',
                'nama_himpunan'=>'Kurang',
            ],
            [
                'id_kriteria'=>'2',
                'range'=>'0',
                'nilai'=>'1',
                'nama_himpunan'=>'Sangat Baik',
            ],
            // --------------
            [
                'id_kriteria'=>'3',
                'range'=>'<100',
                'nilai'=>'0',
                'nama_himpunan'=>'Rendah',
            ],
            [
                'id_kriteria'=>'3',
                'range'=>'100-120',
                'nilai'=>'0.25',
                'nama_himpunan'=>'Kurang',
            ],
            [
                'id_kriteria'=>'3',
                'range'=>'100-120',
                'nilai'=>'0.25',
                'nama_himpunan'=>'Rendah',
            ],
            [
                'id_kriteria'=>'3',
                'range'=>'<100',
                'nilai'=>'0',
                'nama_himpunan'=>'Rendah',
            ],
            [
                'id_kriteria'=>'3',
                'range'=>'<100',
                'nilai'=>'0',
                'nama_himpunan'=>'Rendah',
            ],







        ];

        foreach($data as $data){
            Bobot::insert($data);
        }
    }
}
