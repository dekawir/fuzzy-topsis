<?php

namespace App\Imports;

use App\Models\DataTraining;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;


class ImportDataTraining implements ToModel, WithHeadingRow, WithValidation
{
   
    use Importable;
    public function rules(): array
        {
            return [
                'nama_kelompok' => 'required',
                'alamat' => 'required|max:100',
                'kecamatan' => 'required',
                'nama_ketua_kelompok' => 'required',

                'jumlah_anggota' => 'required',
                'riwayat_bantuan' => 'required|numeric',
                'luas_lahan' => 'required|numeric',
                'keaktifan_kelompok' => 'required|numeric|min:1|max:5',
                'lokasi_kebun' => 'required|numeric',
            ];

        }

        public function customValidationMessages()
        {
            return [
                'nama_kelompok' => 'Nama Kelompok masih kosong !!!',
                'alamat' => 'Alamat masih kosong !!!',
                'kecamatan' => 'Kecamatan masih kosong !!!',
                'nama_ketua_kelompok' =>'Nama Ketua Kelompok masih kosong !!!',

                'jumlah_anggota' => 'Jumlah Anggota masih kosong !!!',
                'riwayat_bantuan' => 'Riwayat Bantuan masih kosong !!!',
                'luas_lahan' => 'Luas Lahan masih kosong !!!',
                'keaktifan_kelompok' => 'Keaktifan Kelompok masih kosong !!!',
                'lokasi_kebun' => 'Lokasi Kebun masih kosong !!!',
            ];
        }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    public function model(array $row)
    {
        // dd($row);
        return new DataTraining([
            'nama_kelompok'=>$row['nama_kelompok'],
            'alamat'=>$row['alamat'],
            'kecamatan'=>$row['kecamatan'],
            'nama_ketua_kelompok'=>$row['nama_ketua_kelompok'],
            
            
            'jumlah_anggota'=>$row['jumlah_anggota'],
            'riwayat_bantuan'=>$row['riwayat_bantuan'],
            'luas_lahan'=>$row['luas_lahan'],
            'keaktifan_kelompok'=>$row['keaktifan_kelompok'],
            'lokasi_kebun'=>$row['lokasi_kebun'],
            'keterangan'=>$row['keterangan'],
        ]);
    }
}
