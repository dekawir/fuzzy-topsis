<?php

namespace App\Http\Controllers;


use App\Imports\ImportDataTraining;
use App\Models\DataTraining;
use App\Models\Kriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class UjiAkurasiController extends Controller
{
    public function index(Request $request)
    {

        if($request->post()){
            $validator = Validator::make($request->all(),[
                'persentase' => 'required|numeric',
                'testing' => 'required'
            ]);

            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            $total = DataTraining::count();
            // mencari persentase
            $persentase = $request->persentase * $total/100;
            $floatPersentase = floor($persentase);
            // $sisaPersentase = $total-$floatPersentase;
            $persen = "limit $floatPersentase";  //keseluruhan data testing
    
            // dd($persen);
            if($request->post('testing')==="Urut"){
                $testing = "";
            }
            if($request->post('testing')==="Acak"){
                $testing = "rand";
            }
    
            $alternatif = DB::select('select * from data_training order by '.$testing.' (id) '.$persen.' ');
            
            if(empty($alternatif)){
                $nominasi[]='';
                return back()->with('error','Data Training kosong!!!');
            }
    
                foreach($alternatif as $key=>$a){
                    $nominasi[] = [
                        'a'=> 'A'.$key + 1,
                        'jumlah_anggota'=>$a->jumlah_anggota,
                        'riwayat_bantuan'=>$a->riwayat_bantuan,
                        'luas_lahan'=>$a->luas_lahan,
                        'keaktifan_kelompok'=>$a->keaktifan_kelompok,
                        'lokasi_kebun'=>$a->lokasi_kebun,
                    ]; 
                }
    
    
            $sum_jumlah_anggota=0;
            $sum_riwayat_bantuan=0;
            $sum_luas_lahan=0;
            $sum_keaktifan_kelompok=0;
            $sum_lokasi_kebun=0;
        }


        if($request->post()){
            $alternatif = DB::select('select * from data_training order by '.$testing.' (id) '.$persen.' ');


            foreach($alternatif as $key=>$a){
                
                
                // jumlah anggota
                if($alternatif[$key]->jumlah_anggota<=10){
                    $fuzzy_jumlah_anggota = 'R';
                }elseif($alternatif[$key]->jumlah_anggota>10 && $alternatif[$key]->jumlah_anggota <=15 ){
                    $fuzzy_jumlah_anggota = 'K';
                }elseif($alternatif[$key]->jumlah_anggota>15 && $alternatif[$key]->jumlah_anggota <=20){
                    $fuzzy_jumlah_anggota = 'CB';
                }elseif($alternatif[$key]->jumlah_anggota>20 && $alternatif[$key]->jumlah_anggota <=25){
                    $fuzzy_jumlah_anggota = 'B';
                }elseif($alternatif[$key]->jumlah_anggota>26){
                    $fuzzy_jumlah_anggota = 'SB';
                }
                
                // riwayat bantuan
                if($alternatif[$key]->riwayat_bantuan>=2){
                    $fuzzy_riwayat_bantuan = 'R';
                }elseif($alternatif[$key]->riwayat_bantuan==1 ){
                    $fuzzy_riwayat_bantuan = 'K';
                }elseif($alternatif[$key]->riwayat_bantuan<1){
                    $fuzzy_riwayat_bantuan = 'SB';
                }
                
                // luas lahan
                if($alternatif[$key]->luas_lahan<=100){
                    $fuzzy_luas_lahan = 'R';
                }elseif($alternatif[$key]->luas_lahan>100 && $alternatif[$key]->luas_lahan <=120 ){
                    $fuzzy_luas_lahan = 'K';
                }elseif($alternatif[$key]->luas_lahan>120 && $alternatif[$key]->luas_lahan <150){
                    $fuzzy_luas_lahan = 'CB';
                }elseif($alternatif[$key]->luas_lahan>150 && $alternatif[$key]->luas_lahan <=200){
                    $fuzzy_luas_lahan = 'B';
                }elseif($alternatif[$key]->luas_lahan>200){
                    $fuzzy_luas_lahan = 'SB';
                }

                // keaktifan kelompok
                if($alternatif[$key]->keaktifan_kelompok==5){
                    $fuzzy_keaktifan_kelompok = 'R';
                }elseif($alternatif[$key]->keaktifan_kelompok==4 ){
                    $fuzzy_keaktifan_kelompok = 'K';
                }elseif($alternatif[$key]->keaktifan_kelompok==3 ){
                    $fuzzy_keaktifan_kelompok = 'CB';
                }elseif($alternatif[$key]->keaktifan_kelompok==2 ){
                    $fuzzy_keaktifan_kelompok = 'B';
                }elseif($alternatif[$key]->keaktifan_kelompok==1){
                    $fuzzy_keaktifan_kelompok = 'SB';
                }

                // lokasi kebun
                if($alternatif[$key]->lokasi_kebun==0){
                    $fuzzy_lokasi_kebun = 'R';
                }else{
                    $fuzzy_lokasi_kebun = 'SB';
                }

                $fuzzy[] = [
                    'a'=> 'A'.$key + 1,
                    'jumlah_anggota'=>$fuzzy_jumlah_anggota,
                    'riwayat_bantuan'=>$fuzzy_riwayat_bantuan,
                    'luas_lahan'=>$fuzzy_luas_lahan,
                    'keaktifan_kelompok'=>$fuzzy_keaktifan_kelompok,
                    'lokasi_kebun'=>$fuzzy_lokasi_kebun,
                ]; 


                // ---defuzzifikasi_fuzzy_topsis

                // jumlah anggota
                if($alternatif[$key]->jumlah_anggota<=10){
                    $difuzzy_jumlah_anggota = '0';
                }elseif($alternatif[$key]->jumlah_anggota>10 && $alternatif[$key]->jumlah_anggota <=15 ){
                    $difuzzy_jumlah_anggota = '0.25';
                }elseif($alternatif[$key]->jumlah_anggota>15 && $alternatif[$key]->jumlah_anggota <=20){
                    $difuzzy_jumlah_anggota = '0.50';
                }elseif($alternatif[$key]->jumlah_anggota>20 && $alternatif[$key]->jumlah_anggota <=25){
                    $difuzzy_jumlah_anggota = '0.75';
                }elseif($alternatif[$key]->jumlah_anggota>26){
                    $difuzzy_jumlah_anggota = '1';
                }
                
                // riwayat bantuan
                if($alternatif[$key]->riwayat_bantuan>=2){
                    $difuzzy_riwayat_bantuan = '0';
                }elseif($alternatif[$key]->riwayat_bantuan==1 ){
                    $difuzzy_riwayat_bantuan = '0.25';
                }elseif($alternatif[$key]->riwayat_bantuan<1){
                    $difuzzy_riwayat_bantuan = '1';
                }
                
                // luas lahan
                if($alternatif[$key]->luas_lahan<=100){
                    $difuzzy_luas_lahan = '0';
                }elseif($alternatif[$key]->luas_lahan>100 && $alternatif[$key]->luas_lahan <=120 ){
                    $difuzzy_luas_lahan = '0.25';
                }elseif($alternatif[$key]->luas_lahan>120 && $alternatif[$key]->luas_lahan <150){
                    $difuzzy_luas_lahan = '0.50';
                }elseif($alternatif[$key]->luas_lahan>150 && $alternatif[$key]->luas_lahan <=200){
                    $difuzzy_luas_lahan = '0.75';
                }elseif($alternatif[$key]->luas_lahan>200){
                    $difuzzy_luas_lahan = '1';
                }

                // keaktifan kelompok
                if($alternatif[$key]->keaktifan_kelompok==5){
                    $difuzzy_keaktifan_kelompok = '0';
                }elseif($alternatif[$key]->keaktifan_kelompok==4 ){
                    $difuzzy_keaktifan_kelompok = '0.25';
                }elseif($alternatif[$key]->keaktifan_kelompok==3 ){
                    $difuzzy_keaktifan_kelompok = '0.50';
                }elseif($alternatif[$key]->keaktifan_kelompok==2 ){
                    $difuzzy_keaktifan_kelompok = '0.75';
                }elseif($alternatif[$key]->keaktifan_kelompok==1){
                    $difuzzy_keaktifan_kelompok = '1';
                }

                // lokasi kebun
                if($alternatif[$key]->lokasi_kebun==0){
                    $difuzzy_lokasi_kebun = '0';
                }else{
                    $difuzzy_lokasi_kebun = '1';
                }

                $defuzzifikasi[] = [
                    'a'=> 'A'.$key + 1,
                    'jumlah_anggota'=>$difuzzy_jumlah_anggota,
                    'riwayat_bantuan'=>$difuzzy_riwayat_bantuan,
                    'luas_lahan'=>$difuzzy_luas_lahan,
                    'keaktifan_kelompok'=>$difuzzy_keaktifan_kelompok,
                    'lokasi_kebun'=>$difuzzy_lokasi_kebun,
                ]; 

                $akar [] = [
                    'a'=> 'A'.$key + 1,
                    'jumlah_anggota'=>pow($difuzzy_jumlah_anggota,2),
                    'riwayat_bantuan'=>pow($difuzzy_riwayat_bantuan,2),
                    'luas_lahan'=>pow($difuzzy_luas_lahan,2),
                    'keaktifan_kelompok'=>pow($difuzzy_keaktifan_kelompok,2),
                    'lokasi_kebun'=>pow($difuzzy_lokasi_kebun,2),
                ];

                $sum_jumlah_anggota+=$akar[$key]['jumlah_anggota'];
                $sum_riwayat_bantuan+=$akar[$key]['riwayat_bantuan'];
                $sum_luas_lahan+=$akar[$key]['luas_lahan'];
                $sum_keaktifan_kelompok+=$akar[$key]['keaktifan_kelompok'];
                $sum_lokasi_kebun+=$akar[$key]['lokasi_kebun'];
                
                
            }
            
            foreach($defuzzifikasi as $k=>$def){
                
                $nor[] = [
                    'a'=> 'A'.$k+1,
                    'jumlah_anggota'=>sprintf('%f',$def['jumlah_anggota']/sqrt($sum_jumlah_anggota)),
                    'riwayat_bantuan'=>sprintf('%f',$def['riwayat_bantuan']/sqrt($sum_riwayat_bantuan)),
                    'luas_lahan'=>sprintf('%f',$def['luas_lahan']/sqrt($sum_luas_lahan)),
                    'keaktifan_kelompok'=>sprintf('%f',$def['keaktifan_kelompok']/sqrt($sum_keaktifan_kelompok)),
                    'lokasi_kebun'=>sprintf('%f',$def['lokasi_kebun']/sqrt($sum_lokasi_kebun)),
                ];
            }
            
            
            foreach($nor as $no=>$n){
                $bobot_fuzzy[] = [
                    'a'=> 'A'.$no +1,
                    'jumlah_anggota'=>sprintf('%f',$n['jumlah_anggota']*Kriteria::get()[0]->bobot),
                    'riwayat_bantuan'=>sprintf('%f',$n['riwayat_bantuan']*Kriteria::get()[1]->bobot),
                    'luas_lahan'=>sprintf('%f',$n['luas_lahan']*Kriteria::get()[2]->bobot),
                    'keaktifan_kelompok'=>sprintf('%f',$n['keaktifan_kelompok']*Kriteria::get()[3]->bobot),
                    'lokasi_kebun'=>sprintf('%f',$n['lokasi_kebun']*Kriteria::get()[4]->bobot),
                ];
            }

           
            foreach($bobot_fuzzy as $key_fuz=>$f){
                $arr_jumlah_anggota[] = array_fill(0,1,$f['jumlah_anggota']);
                $arr_riwayat_bantuan[] = array_fill(0,1,$f['riwayat_bantuan']);
                $arr_luas_lahan[] = array_fill(0,1,$f['luas_lahan']);
                $arr_keaktifan_kelompok[] = array_fill(0,1,$f['keaktifan_kelompok']);
                $arr_lokasi_kebun[] = array_fill(0,1,$f['lokasi_kebun']);

            }
            $solusi = [
                [
                    'a'=> "+",
                    'c1'=> max($arr_jumlah_anggota),
                    'c2'=> max($arr_riwayat_bantuan),
                    'c3'=> max($arr_luas_lahan),
                    'c4'=> max($arr_keaktifan_kelompok),
                    'c5'=> max($arr_lokasi_kebun),
                ],
                [
                    'a'=> '-',
                    'c1'=> min($arr_jumlah_anggota),
                    'c2'=> min($arr_riwayat_bantuan),
                    'c3'=> min($arr_luas_lahan),
                    'c4'=> min($arr_keaktifan_kelompok),
                    'c5'=> min($arr_lokasi_kebun),
                ],
                
            ];

            // dd($solusi[0]['c1'][0]);
            foreach($bobot_fuzzy as $b_key=>$b_fuzzy){
                $alternatif_positif[]  = [
                    'a'=>$b_fuzzy['a'],
                    'c1'=> pow($solusi[0]['c1'][0] - $b_fuzzy['jumlah_anggota'],2),
                    'c2'=> pow($solusi[0]['c2'][0] - $b_fuzzy['riwayat_bantuan'],2),
                    'c3'=> pow($solusi[0]['c3'][0] - $b_fuzzy['luas_lahan'],2),
                    'c4'=> pow($solusi[0]['c4'][0] - $b_fuzzy['keaktifan_kelompok'],2),
                    'c5'=> pow($solusi[0]['c5'][0] - $b_fuzzy['lokasi_kebun'],2),
                ];
            }
            // dd($alternatif_postif);

            foreach($bobot_fuzzy as $b_key=>$b_fuzzy){
                $alternatif_negatif[]  = [
                    'a'=>$b_fuzzy['a'],
                    'c1'=> pow($solusi[1]['c1'][0] - $b_fuzzy['jumlah_anggota'],2),
                    'c2'=> pow($solusi[1]['c2'][0] - $b_fuzzy['riwayat_bantuan'],2),
                    'c3'=> pow($solusi[1]['c3'][0] - $b_fuzzy['luas_lahan'],2),
                    'c4'=> pow($solusi[1]['c4'][0] - $b_fuzzy['keaktifan_kelompok'],2),
                    'c5'=> pow($solusi[1]['c5'][0] - $b_fuzzy['lokasi_kebun'],2),
                ];
            }

            foreach ($alternatif_positif as $key_pos=>$positif){
                $dplus[]  = [
                    'a'=> 'D+'.$key_pos +1,
                    'plus' => 
                    $alternatif_positif[$key_pos]['c1']+
                    $alternatif_positif[$key_pos]['c2']+
                    $alternatif_positif[$key_pos]['c3']+
                    $alternatif_positif[$key_pos]['c4']+
                    $alternatif_positif[$key_pos]['c5'],
                    'pangkat'=> sqrt(
                        $alternatif_positif[$key_pos]['c1']+
                        $alternatif_positif[$key_pos]['c2']+
                        $alternatif_positif[$key_pos]['c3']+
                        $alternatif_positif[$key_pos]['c4']+
                        $alternatif_positif[$key_pos]['c5']
                    ),

                ] ;

                $dmin[]  = [
                    'a'=> 'D-'.$key_pos +1,
                    'min' => 
                    $alternatif_negatif[$key_pos]['c1']+
                    $alternatif_negatif[$key_pos]['c2']+
                    $alternatif_negatif[$key_pos]['c3']+
                    $alternatif_negatif[$key_pos]['c4']+
                    $alternatif_negatif[$key_pos]['c5'],
                    'pangkat'=> sqrt(
                        $alternatif_negatif[$key_pos]['c1']+
                        $alternatif_negatif[$key_pos]['c2']+
                        $alternatif_negatif[$key_pos]['c3']+
                        $alternatif_negatif[$key_pos]['c4']+
                        $alternatif_negatif[$key_pos]['c5']
                    ),

                ] ;
            }
            

            foreach($dplus as $key_vi=>$v){
                if(($dmin[$key_vi]['pangkat']/($dmin[$key_vi]['pangkat']+$dplus[$key_vi]['pangkat']))>0.50 && 
                    $dmin[$key_vi]['pangkat']/($dmin[$key_vi]['pangkat']+$dplus[$key_vi]['pangkat'])<=1){
                        $grade="Lolos";
                    }else{
                        $grade="Tidak Lolos";

                    }
                $vi []= [
                    'v'=> 'V'.$key_vi+1,
                    'training'=> $alternatif[$key_vi]->keterangan,
                    'vi'=>$dmin[$key_vi]['pangkat']/($dmin[$key_vi]['pangkat']+$dplus[$key_vi]['pangkat']),
                    'grade'=>$grade
                ];
            }
            // dd($vi);

            $rank = collect($vi)->sortBy('vi')->reverse()->toArray();
            $no=1;
            foreach($rank as $key=>$r){
                $ranking[] = [
                    'v'=> $r['v'],
                    'vi'=> $r['vi'],
                    'rank'=> $no++
                ];
            }
            
            $TP = 0;
            $TN = 0;
            $FN = 0;
            $FP = 0;
            foreach($vi as $key_v=>$c){
                // dd($c['grade']);
                if($c['training']==="Lolos" && $c['grade']==="Lolos"){
                    $conf="TP";
                    $TP+=1;
                }elseif($c['training']==="Lolos" && $c['grade']==="Tidak Lolos"){
                    $conf="FP";  
                    $FP+=1;  
                }elseif($c['training']==="Tidak Lolos" && $c['grade']==="Lolos"){
                    $conf="FN";    
                    $FN+=1;  
                }elseif($c['training']==="Tidak Lolos" && $c['grade']==="Tidak Lolos"){
                    $conf="TN";    
                    $TN+=1;  
                }

            }
            // dd($TP,$FN,$TN,$FP);
            $confusion=[
                'TP'=>$TP,
                'FP'=>$FP,
                'FN'=>$FN,
                'TN'=>$TN,
            ];
            // dd($confusion);

            return view('uji-akurasi.uji-akurasi',[
                'title'=>'Uji Akurasi',
                'nominasi'=> $nominasi,
                'fuzzy'=> $fuzzy,
                'difuzzy'=> $defuzzifikasi,
                'normalisasi_matrik_fuzzy'=> $nor,
                'bobot_fuzzy'=> $bobot_fuzzy,
                'solusi'=> $solusi,
                'alternatif_positif'=> $alternatif_positif,
                'alternatif_negatif'=> $alternatif_negatif,
                'dplus'=> $dplus,
                'dmin'=> $dmin,
                'rank'=> $ranking,
                'hasil'=> $vi,
                'conf'=> $confusion,
            ]);
        }    

        
       
        return view('uji-akurasi.uji-akurasi',[
            'title'=>'Uji Akurasi',
            // 'nominasi'=> $nominasi,
            'fuzzy'=> '',
            'difuzzy'=> '',
            'normalisasi_matrik_fuzzy'=> '',
        ]);
    }
}
