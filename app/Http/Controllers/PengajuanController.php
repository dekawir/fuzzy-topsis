<?php

namespace App\Http\Controllers;

use App\Models\Pengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PengajuanController extends Controller
{
    public function index(Request $request)
    {
        if($request->post()){
            
            $validator = Validator::make($request->all(), [
                'nama_kelompok' => 'required',
                'alamat' => 'required|max:100',
                'kecamatan' => 'required',
                'nama_ketua_kelompok' => 'required',

                'jumlah_anggota' => 'required',
                'riwayat_bantuan' => 'required|numeric',
                'luas_lahan' => 'required|numeric',
                'keaktifan_kelompok' => 'required|numeric|min:1|max:5',
                'lokasi_kebun' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            
            $data = [
                'nama_kelompok'=>$request->post('nama_kelompok'),
                'alamat'=>$request->post('alamat'),
                'kecamatan'=>$request->post('kecamatan'),
                'nama_ketua_kelompok'=>$request->post('nama_ketua_kelompok'),
                'tanggal_pengajuan'=>date('Y-m-d'),
                
                'jumlah_anggota'=>$request->post('jumlah_anggota'),
                'riwayat_bantuan'=>$request->post('riwayat_bantuan'),
                'luas_lahan'=>$request->post('luas_lahan'),
                'keaktifan_kelompok'=>$request->post('keaktifan_kelompok'),
                'lokasi_kebun'=>$request->post('lokasi_kebun'),
            ];
            
            Pengajuan::insert($data);
            return back()->with('success','Pengajuan ditambahkan');
        }
        // dd($request->session()->all());
        return view('pengajuan-bantuan.pengajuan-bantuan',[
            'title'=>'Pengajuan Bantuan',
            'pengajuan'=>Pengajuan::all(),
        ]);
    }

    public function update(Request $request)
    {
        if($request->post()){

            $validator = Validator::make($request->all(), [
                'nama_kelompok' => 'required',
                'alamat' => 'required|max:100',
                'kecamatan' => 'required',
                'nama_ketua_kelompok' => 'required',

                'jumlah_anggota' => 'required',
                'riwayat_bantuan' => 'required|numeric',
                'luas_lahan' => 'required|numeric',
                'keaktifan_kelompok' => 'required|numeric|min:1|max:5',
                'lokasi_kebun' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            
            $data = [
                'nama_kelompok'=>$request->post('nama_kelompok'),
                'alamat'=>$request->post('alamat'),
                'kecamatan'=>$request->post('kecamatan'),
                'nama_ketua_kelompok'=>$request->post('nama_ketua_kelompok'),
                'tanggal_pengajuan'=>date('Y-m-d'),
                
                'jumlah_anggota'=>$request->post('jumlah_anggota'),
                'riwayat_bantuan'=>$request->post('riwayat_bantuan'),
                'luas_lahan'=>$request->post('luas_lahan'),
                'keaktifan_kelompok'=>$request->post('keaktifan_kelompok'),
                'lokasi_kebun'=>$request->post('lokasi_kebun'),
            ];

            Pengajuan::where('id',$_GET['pengajuan'])->update($data);
            return redirect('pengajuan-bantuan')->with('success','Pengajuan diubah');
        }
        return view('pengajuan-bantuan.pengajuan-bantuan-edit',[
            'title'=> 'Ubah Pengajuan Bantuan',
            'p'=> Pengajuan::where('id',$_GET['pengajuan'])->first(),

        ]);
    }

    public function destroy(Request $request)
    {
        Pengajuan::where('id', $request->id)->delete();
        return back()->with('success','Pengajuan dihapus');
    }
}
