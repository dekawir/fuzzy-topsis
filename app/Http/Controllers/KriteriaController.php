<?php

namespace App\Http\Controllers;

use App\Models\Kriteria;
use App\Models\Pengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KriteriaController extends Controller
{
    public function index(Request $request)
    {
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                'nama_kriteria' => 'required',
                'cost_benefit' => 'required',
                'bobot' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            
            $data = [
                'nama_kriteria'=>$request->post('nama_kriteria'),
                'cost_benefit'=>$request->post('cost_benefit'),
                'bobot'=>$request->post('bobot'),
            ];
            
            Kriteria::insert($data);
            return back()->with('success','Pengajuan ditambahkan');
        }
        return view('kriteria.kriteria',[
            'title'=>'Data Kriteria',
            'kriteria'=>Kriteria::all(),
        ]);
    }

    public function update(Request $request)
    {
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                'nama_kriteria' => 'required',
                'cost_benefit' => 'required',
                'bobot' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            
            $data = [
                'nama_kriteria'=>$request->post('nama_kriteria'),
                'cost_benefit'=>$request->post('cost_benefit'),
                'bobot'=>$request->post('bobot'),
            ];
            
            Kriteria::where('id',$_GET['kriteria'])->update($data);
            return redirect('kriteria')->with('success','Pengajuan diubah');
        }

        return view('kriteria.kriteria-edit',[
            'title'=>'Data Kriteria',
            'k'=>Kriteria::where('id',$_GET['kriteria'])->first(),
        ]);
    }
    
    public function update_bobot(Request $request)
    {
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                'nama_kriteria' => 'required',
                'cost_benefit' => 'required',
                'bobot' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            
            $data = [
                'nama_kriteria'=>$request->post('nama_kriteria'),
                'cost_benefit'=>$request->post('cost_benefit'),
                'bobot'=>$request->post('bobot'),
            ];
            
            Kriteria::where('id',$_GET['kriteria'])->update($data);
            return redirect('kriteria')->with('success','Pengajuan diubah');
        }

        return view('kriteria.kriteria-bobot',[
            'title'=>'Nilai Bobot Kriteria',
            'kriteria'=>Kriteria::all(),
        ]);
    }
}
