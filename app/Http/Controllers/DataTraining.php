<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ImportDataTraining;
use App\Models\DataTraining as ModelsDataTraining;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class DataTraining extends Controller
{
    public function index(Request $request)
    {
        if($request->post('kosongkan')){
            ModelsDataTraining::truncate();
            return back()->with('success','Data Berhasil Dikosongkan');
            
        }

        if($request->post())
        {
            $validator = Validator::make($request->all(),[
                'file' => 'required|mimes:csv,xls,xlsx',
            ]);
            
            // dd($request->all());
            if($validator->fails()){
                return back()->withInput()->withErrors($validator);
            }

            $import = Excel::import(new ImportDataTraining, $request->file('file'));
            
            return back()->with('success','Data Berhasil Diimport');
            
        }
        


        return view('data_training.data_training',[
            'title'=>'Data Training',
            'data' => ModelsDataTraining::all(),
        
        ]);
    }
}
