<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'pengajuan_bantuan';
    protected $guarded = 'id';
}
