<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataTraining extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'data_training';
    protected $guarded = ['id'];
}
