<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DataTraining;
use App\Http\Controllers\KriteriaController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PengajuanController;
use App\Http\Controllers\PerhitunganController;
use App\Http\Controllers\UjiAkurasiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/login',[LoginController::class,'index'])->name('login');
Route::post('/login',[LoginController::class,'index']);


Route::get('/dashboard',[DashboardController::class,'index'])->name('dashboard');
Route::get('/pengajuan-bantuan',[PengajuanController::class,'index']);
Route::post('/pengajuan-bantuan',[PengajuanController::class,'index']);
Route::get('/pengajuan-bantuan-ubah',[PengajuanController::class,'update']);
Route::post('/pengajuan-bantuan-ubah',[PengajuanController::class,'update']);
Route::post('/pengajuan-bantuan-hapus',[PengajuanController::class,'destroy'])->name('del.pengajuan');

Route::get('/kriteria',[KriteriaController::class,'index']);
Route::get('/kriteria-ubah',[KriteriaController::class,'update']);
Route::post('/kriteria-ubah',[KriteriaController::class,'update']);
Route::get('/kriteria-bobot',[KriteriaController::class,'update_bobot']);
Route::post('/kriteria-bobot',[KriteriaController::class,'update_bobot']);

Route::get('/perhitungan',[PerhitunganController::class,'index']);
Route::post('/perhitungan',[PerhitunganController::class,'index']);

Route::get('/data-training',[DataTraining::class,'index']);
Route::post('/data-training',[DataTraining::class,'index']);

Route::get('/uji-akurasi',[UjiAkurasiController::class,'index']);
Route::post('/uji-akurasi',[UjiAkurasiController::class,'index']);

Route::get('/hasil',[PerhitunganController::class,'hasil']);
Route::get('/laporan',[PerhitunganController::class,'laporan']);
Route::get('/laporan-print',[PerhitunganController::class,'print']);

Route::get('/nominasi-matrik-berpasangan',[PerhitunganController::class,'nominasi_matrik_berpasangan']);
Route::get('/defuzzifikasi-fuzzy-topsis',[PerhitunganController::class,'defuzzifikasi_fuzzy_topsis']);
Route::get('/normalisasi-matriks-fuzzy-topsis',[PerhitunganController::class,'normalisasi_matriks_fuzzy_topsis']);


Route::post('/logout',[LoginController::class,'logout'])->name('logout');
