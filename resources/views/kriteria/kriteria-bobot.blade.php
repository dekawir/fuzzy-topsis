@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>


	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/components_modals.js')}}"></script> --}}
	<!-- /theme JS files -->


@endpush
@section('content')
<!-- Content area -->
<div class="content">

    <!-- Form horizontal -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            {{-- <h5 class="panel-title">Basic form inputs</h5> --}}
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- <p class="content-group-lg">Examples of standard form controls supported in an example form layout. Individual form controls automatically receive some global styling. All textual <code>&lt;input></code>, <code>&lt;textarea></code>, and <code>&lt;select></code> elements with <code>.form-control</code> are set to <code>width: 100%;</code> by default. Wrap labels and controls in <code>.form-group</code> for optimum spacing. Labels in horizontal form require <code>.control-label</code> class.</p> --}}

            {{-- <form class="form-horizontal" action="#" method="POST">
                @csrf
                <fieldset class="content-group">
                    <legend class="text-bold">Input Data Kriteria</legend>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama Kriteria</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="nama_kelompok" value="{{ old('nama_kriteria') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Cost/Benefit</label>
                        <div class="col-lg-10">
                            <select class="select-search" name="cost_benefit">
                                <optgroup label="Cost/Benefit">
                                    <option value="">Pilih</option>
                                    <option value="Cost" {{ old('cost_benefit')=='Cost'?'selected':'' }}>Cost</option>
                                    <option value="Benefit" {{ old('cost_benefit')=='Benefit'?'selected':'' }}>Benefit</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Bobot</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="bobot" value="{{ old('bobot') }}" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>

                    
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form> --}}

            <legend class="text-bold">Data Kriteria</legend>

            <table class="table datatable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kriteria</th>
                        <th>Cost/Benefit</th>
                        <th>Bobot</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kriteria as $key => $p)    
                    <tr>
                        <td>C {{ $key+1 }}</td>
                        <td>{{ $p->nama_kriteria }}</td>
                        <td>
                            @if($p->cost_benefit==="Benefit")
                            <span class="label label-success">{{ ($p->cost_benefit) }}</span>
                            @else
                            <span class="label label-primary">{{ ($p->cost_benefit) }}</span>
                            @endif
                        </td>
                        <td>{{ $p->bobot }}</td>
                        <td class="text-center">
                            <a href="kriteria-ubah?kriteria={{ $p->id }}" type="button" class="btn btn-info btn-xs"><i class="icon-pencil7"></i> Edit</a>
                            <a href="kriteria-bobot?kriteria={{ $p->id }}" type="button" class="btn btn-warning btn-xs"><i class="icon-list-numbered"></i> Bobot</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /form horizontal -->
</div>
<!-- /content area -->

<!-- /primary modal -->
@endsection