<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan</title>

    <style>
        
        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
        }

        .text-center{
            text-align: center
        }

        .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        }

        /* @media print { */
        .bg-success{
            background-color: green;
        }
    /* } */

    /* @media print { */

        .bg-danger{
            background-color: red;
        }
    /* } */
        
        </style>
</head>
<body>
    <div class="row">
        <div class="">
            <img src="{{ asset('kop.jpg') }}" alt="" class="center">
            <h4 class="text-bold"> REKAPITULASI BANTUAN KE MASYARAKAT/KELOMPOK TAHUN 2021-2022 BANTUAN PEKARANGAN PANGAN LESTARI (P2L)</h4>
            <div class="table-responsive">
                <div>
                </div>

                <table >
                    <thead>
                        <tr class="bg-blue">
                            <th rowspan="2" class="text-center">NO</th>
                            <th rowspan="2" class="text-center">NAMA ORGANISASI / KELOMPOK MASYARAKAT PENERIMA </th>
                            <th rowspan="2" class="text-center">ALAMAT</th>
                            <th rowspan="2" class="text-center">KECAMATAN</th>
                            <th rowspan="2" class="text-center">NAMA KETUA KELOMPOK</th>
                            <th colspan="5" class="text-center">KRITERIA KEGIATAN P2L</th>
                            <th rowspan="2" class="text-center">KETERANGAN</th>
                        </tr>
                        <tr class="bg-blue">
                            <th rowspan="" class="text-center">JUMLAH ANGGOTA</th>
                            <th rowspan="" class="text-center">RIWAYAT BANTUAN PEMERINTAH</th>
                            <th colspan="" class="text-center">LUAS LAHAN (M2)</th>
                            <th colspan="" class="text-center">KEAKTIFAN KELOMPOK</th>
                            <th colspan="" class="text-center">LOKASI  KEBUN DEKAT DENGAN FASILITAS TRANSPORTASI </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($laporan as $key=> $l)
                        <tr>
                            <td class="text-center">{{ $key +1 }}</td>
                            <td class="text-center">{{ $l['nama_kelompok'] }}</td>
                            <td class="text-center">{{ $l['alamat'] }}</td>
                            <td class="text-center">{{ $l['kecamatan'] }}</td>
                            <td class="text-center">{{ $l['nama_ketua_kelompok'] }}</td>
                            <td class="text-center">{{ $l['jumlah_anggota'] }}</td>
                            <td class="text-center">{{ $l['riwayat_bantuan'] }}</td>
                            <td class="text-center">{{ $l['luas_lahan'] }}</td>
                            <td class="text-center">{{ $l['keaktifan_kelompok'] }}</td>
                            <td class="text-center">{{ $l['lokasi_kebun'] }}</td>
                            <td class="text-center">
                                @if($l['grade']>0.50 && $l['grade'] <=1)  <span class="bg-success">Lolos</span>
                                @else <span class="bg-danger">Tidak Lolos</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>