<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name') }} | {{ $title }}</title>

	<link rel="shortcut icon" href="{{ asset('img/icon.png') }}">


	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/minified/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/minified/core.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/minified/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/minified/colors.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/components_notifications_pnotify.js') }}"></script> --}}
	<!-- /theme JS files -->

	<!-- Theme JS files -->

	@stack('asset')

	<!-- /theme JS files -->
	

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="/"><img src="{{ asset('img/logo.png') }}" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

				
			</ul>
			<p class="navbar-text"><span class="label bg-success-400">Online</span></p>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ asset('logo.png') }}" alt="">
						<span>@guest Guest @else {{ Auth::user()->nama }} @endguest</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
						<li>
							<a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i>
                                        {{ __('Logout') }}
                            </a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
								@csrf
							</form>
							
						</li>
						
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="{{ asset('logo.png') }}" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">@guest Guest @else {{ Auth::user()->nama }} @endguest </span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Fuzzy Topsis 
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Menu</span> <i class="icon-menu" title="Main pages"></i></li>
								{{-- <li class="{{ ($title === "Dashboard")? 'active': '' }} "><a href="/dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li> --}}
								<li class="{{ ($title === "Pengajuan Bantuan")? 'active': '' }} "><a href="/pengajuan-bantuan"><i class="icon-pencil5"></i> <span>Pengajuan Bantuan/Alternatif</span></a></li>
								<li class="{{ ($title === "Data Kriteria")? 'active': '' }}{{ ($title === "Nilai Bobot Kriteria")? 'active': '' }}"><a href="kriteria"><i class="icon-stack2"></i>Data Kriteria</a></li>
								<li class="{{ ($title === "Data Training")? 'active': '' }}"><a href="data-training"><i class="icon-database-diff"></i>Data Training</a></li>
								<li class="{{ ($title === "Uji Akurasi")? 'active': '' }}"><a href="uji-akurasi"><i class="icon-bucket"></i>Uji Akurasi</a></li>
								<li class="{{ ($title === "Perhitungan")? 'active': '' }}"><a href="perhitungan"><i class="icon-stack-check"></i>Perhitungan</a></li>
								{{-- <li class="{{ ($title === "")? 'active': '' }}">
									<a href="#"><i class="icon-stack-check"></i>  <span>Perhitungan</span></a>
									<ul>
										<li class="{{ ($title === "Nominasi Matriks Berpasangan")? 'active': '' }}"><a href="nominasi-matrik-berpasangan"> <span>Nominasi Matriks Berpasangan</span></a></li>										
										<li class="{{ ($title === "Defuzzifikasi Fuzzy Topsis")? 'active': '' }}"><a href="defuzzifikasi-fuzzy-topsis"> <span>Defuzzifikasi Fuzzy Topsis</span></a></li>										
										<li class="{{ ($title === "Normalisasi Matriks Fuzzy Topsis")? 'active': '' }}"><a href="normalisasi-matriks-fuzzy-topsis"> <span>Normalisasi Matriks Fuzzy Topsis</span></a></li>										
									</ul>
								</li> --}}
								<li class="{{ ($title === "Hasil")? 'active': '' }} "><a href="/hasil"><i class="icon-calculator2"></i> <span>Hasil</span></a></li>
								<li class="{{ ($title === "Laporan")? 'active': '' }} "><a href="/laporan"><i class="icon-magazine"></i> <span>Laporan</span></a></li>
								{{-- <li>
									<a href="#"><i class="icon-copy"></i> <span>Layouts</span></a>
									<ul>
										<li><a href="index.html" id="layout1">Layout 1 <span class="label bg-warning-400">Current</span></a></li>
										<li><a href="../../layout_2/LTR/index.html" id="layout2">Layout 2</a></li>
										<li><a href="../../layout_3/LTR/index.html" id="layout3">Layout 3</a></li>
										<li><a href="../../layout_4/LTR/index.html" id="layout4">Layout 4</a></li>
										<li class="disabled"><a href="../../layout_5/LTR/index.html" id="layout5">Layout 5 <span class="label">Coming soon</span></a></li>
									</ul>
								</li> --}}
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{ $title }} {{ $title ==="Dashboard"?"- Selamat Datang":"" }}</span></h4> 
						</div>

						{{-- <div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div> --}}
					</div>

					<div class="breadcrumb-line" style="background-color: #0193de; padding-top: 50px;">
						{{-- <ul class="breadcrumb">
							<li><a href="/"><i class="icon-home2 position-left"></i> Dashboard</a></li>
							<li class="">{{ $title }}</li>
						</ul> --}}

					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					@yield('content')

					<!-- Footer -->
					<div class="footer text-muted">
						&copy; {{ date('Y') }}. <a href="#">{{ config('app.name') }}</a> by I Made Merta Yasa
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script >
		$(function(){
			
			@if(session('success'))
				new PNotify({
					title: 'Sukses',
					text: '{{ session('success') }}',
					icon: 'icon-file-check2'
				});
			@endif

			@if(session('error'))
			new PNotify({
				title: 'Gagal',
				text: '{{ session('error') }}',
				icon: 'icon-blocked',
				type: 'error'
			});
			@endif

			@if(session('errors'))
			@foreach(session('errors')->all() as $message)
				new PNotify({
					title: 'Gagal',
					text: '{{ $message }}',
					type: 'error'
				});
			@endforeach
			@endif
			

		});


	</script>
	<script>
		function hanyaAngka(evt) {
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
	
			return false;
			return true;
		}
	</script>

</body>
</html>
