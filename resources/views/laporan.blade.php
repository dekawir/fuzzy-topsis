@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_sorting.js')}}"></script>

    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>


	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/components_modals.js')}}"></script> --}}
	<!-- /theme JS files -->


@endpush
@section('content')
<!-- Content area -->
<div class="content">

    <!-- Form horizontal -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a class="btn btn-primary" href="/laporan-print" target="_blank">Cetak</a></a></li>

                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="">
                    <img src="{{ asset('kop.jpg') }}" alt="" style="display: block;
                    margin-left: auto;
                    margin-right: auto;
                    width: 100%;">
                    <h4 class="text-bold"> REKAPITULASI BANTUAN KE MASYARAKAT/KELOMPOK TAHUN 2021-2022 BANTUAN PEKARANGAN PANGAN LESTARI (P2L)</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered" >
                            <thead>
                                <tr class="bg-blue">
                                    <th rowspan="2" class="text-center">NO</th>
                                    <th rowspan="2" class="text-center">NAMA ORGANISASI / KELOMPOK MASYARAKAT PENERIMA </th>
                                    <th rowspan="2" class="text-center">ALAMAT</th>
                                    <th rowspan="2" class="text-center">KECAMATAN</th>
                                    <th rowspan="2" class="text-center">NAMA KETUA KELOMPOK</th>
                                    <th colspan="5" class="text-center">KRITERIA KEGIATAN P2L</th>
                                    <th rowspan="2" class="text-center">KETERANGAN</th>
                                </tr>
                                <tr class="bg-blue">
                                    <th rowspan="" class="text-center">JUMLAH ANGGOTA</th>
                                    <th rowspan="" class="text-center">RIWAYAT BANTUAN PEMERINTAH</th>
                                    <th colspan="" class="text-center">LUAS LAHAN (M2)</th>
                                    <th colspan="" class="text-center">KEAKTIFAN KELOMPOK</th>
                                    <th colspan="" class="text-center">LOKASI  KEBUN DEKAT DENGAN FASILITAS TRANSPORTASI </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($laporan as $key=> $l)
                                <tr>
                                    <td class="text-center">{{ $key +1 }}</td>
                                    <td class="text-center">{{ $l['nama_kelompok'] }}</td>
                                    <td class="text-center">{{ $l['alamat'] }}</td>
                                    <td class="text-center">{{ $l['kecamatan'] }}</td>
                                    <td class="text-center">{{ $l['nama_ketua_kelompok'] }}</td>
                                    <td class="text-center">{{ $l['jumlah_anggota'] }}</td>
                                    <td class="text-center">{{ $l['riwayat_bantuan'] }}</td>
                                    <td class="text-center">{{ $l['luas_lahan'] }}</td>
                                    <td class="text-center">{{ $l['keaktifan_kelompok'] }}</td>
                                    <td class="text-center">{{ $l['lokasi_kebun'] }}</td>
                                    <td class="text-center">
                                        @if($l['grade']>0.50 && $l['grade'] <=1)  <span class="bg-success">Lolos</span>
                                        @else <span class="bg-danger">Tidak Lolos</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /form horizontal -->
</div>
<!-- /content area -->


@endsection