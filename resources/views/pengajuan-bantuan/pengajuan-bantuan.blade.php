@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_sorting.js')}}"></script>

    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>


	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/components_modals.js')}}"></script> --}}
	<!-- /theme JS files -->


@endpush
@section('content')
<!-- Content area -->
<div class="content">

    <!-- Form horizontal -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            {{-- <h5 class="panel-title">Basic form inputs</h5> --}}
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- <p class="content-group-lg">Examples of standard form controls supported in an example form layout. Individual form controls automatically receive some global styling. All textual <code>&lt;input></code>, <code>&lt;textarea></code>, and <code>&lt;select></code> elements with <code>.form-control</code> are set to <code>width: 100%;</code> by default. Wrap labels and controls in <code>.form-group</code> for optimum spacing. Labels in horizontal form require <code>.control-label</code> class.</p> --}}

            <form class="form-horizontal" action="#" method="POST">
                @csrf
                <fieldset class="content-group">
                    <legend class="text-bold">Input Data Calon Penerima Bantuan</legend>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama Kelompok</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="nama_kelompok" value="{{ old('nama_kelompok') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat</label>
                        <div class="col-lg-10">
                            <textarea rows="2" cols="5" class="form-control" name="alamat"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Kecamatan</label>
                        <div class="col-lg-10">
                            <select class="select-search" name="kecamatan">
                                <optgroup label="Nama Kecamatan">
                                    <option value="">Pilih</option>
                                    <option value="Denpasar Utara" {{ old('kecamatan')=='Denpasar Utara'?'selected':'' }}>Denpasar Utara</option>
                                    <option value="Denpasar Timur" {{ old('kecamatan')=='Denpasar Timur'?'selected':'' }}>Denpasar Timur</option>
                                    <option value="Denpasar Selatan" {{ old('kecamatan')=='Denpasar Selatan'?'selected':'' }}>Denpasar Selatan</option>
                                    <option value="Denpasar Barat" {{ old('kecamatan')=='Denpasar Barat'?'selected':'' }}>Denpasar Barat</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama Ketua Kelompok</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="nama_ketua_kelompok" value="{{ old('nama_ketua_kelompok') }}">
                        </div>
                    </div>
                    
                    <legend class="text-bold"></legend>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Jumlah Anggota</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="jumlah_anggota" value="{{ old('jumlah_anggota') }}" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Riwayat Bantuan Pemerintah </label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="riwayat_bantuan" value="{{ old('riwayat_bantuan') }}" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Luas Lahan (m2) </label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="luas_lahan" value="{{ old('luas_lahan') }}" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Keaktifan Kelompok</label>
                        <div class="col-lg-10">
                            <select class="select" name="keaktifan_kelompok">
                                <optgroup label="Keaktifan Kelompok">
                                    <option value="">Pilih</option>
                                    <option value="1" {{ old('keaktifan_kelompok')=='1'?'selected':'' }}>Sangat Aktif</option>
                                    <option value="2" {{ old('keaktifan_kelompok')=='2'?'selected':'' }}>Aktif</option>
                                    <option value="3" {{ old('keaktifan_kelompok')=='3'?'selected':'' }}>Cukup Aktif</option>
                                    <option value="4" {{ old('keaktifan_kelompok')=='4'?'selected':'' }}>Kurang Aktif</option>
                                    <option value="5" {{ old('keaktifan_kelompok')=='5'?'selected':'' }}>Tidak Aktif</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Lokasi Kebun Dekat Dengan Fasilitas Transportasi</label>
                        <div class="col-lg-10">
                            <select class="select" name="lokasi_kebun">
                                <optgroup label="Keaktifan Kelompok">
                                    <option value="">Pilih</option>
                                    <option value="1" {{ old('lokasi_kebun')=='1'?'selected':'' }}>Ya</option>
                                    <option value="0" {{ old('lokasi_kebun')=='0'?'selected':'' }}>Tidak</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>

            <legend class="text-bold">Data Calon Penerima Bantuan</legend>

            <table class="table datatable-sorting">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kelompok</th>
                        <th>Nama Ketua</th>
                        <th>Tanggal Pengajuan</th>
                        <th>Keaktifan Kelompok</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pengajuan as $key => $p)    
                    <tr>
                        <td>A {{ $key+1 }}</td>
                        <td>{{ $p->nama_kelompok }}</td>
                        <td>{{ $p->nama_ketua_kelompok }}</td>
                        <td>{{ date('d/m/Y', strtotime($p->tanggal_pengajuan)) }}</td>
                        <td>
                            @if($p->keaktifan_kelompok=='1')<span class="label label-success">Sangat Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='2') <span class="label label-primary"> Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='3') <span class="label label-default">Cukup Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='4') <span class="label label-warning">Kurang Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='5') <span class="label label-danger">Tidak Aktif</span> 
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="pengajuan-bantuan-ubah?pengajuan={{ $p->id }}" type="button" class="btn btn-info btn-xs"><i class="icon-pencil7"></i></a>
                            <button type="button" onclick="del({{ $p->id }})" data-category="{{ $p->id }}"" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal_theme_primary"> <i class="icon-trash"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /form horizontal -->
</div>
<!-- /content area -->

<!-- Primary modal -->
<div id="modal_theme_primary" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('del.pengajuan') }}" method="POST">
                @csrf
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Konfirmasi</h6>
                </div>

                <div class="modal-body">
                    <h6 class="text-semibold">Apakah anda yakin menghapus data ini?</h6>
                    <input type="hidden" id="delUser" name="id">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                    <button type="submit"  class="btn btn-primary">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /primary modal -->

{{-- Skrip tarik data modal confirmation --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delUser");
        input.value = id;
        // console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>
@endsection