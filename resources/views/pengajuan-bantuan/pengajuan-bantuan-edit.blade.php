@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	<!-- /theme JS files -->


@endpush
@section('content')
<!-- Content area -->
<div class="content">

    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title" style="text-align: center">Ubah Data Calon Penerima</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- <p class="content-group-lg">Examples of standard form controls supported in an example form layout. Individual form controls automatically receive some global styling. All textual <code>&lt;input></code>, <code>&lt;textarea></code>, and <code>&lt;select></code> elements with <code>.form-control</code> are set to <code>width: 100%;</code> by default. Wrap labels and controls in <code>.form-group</code> for optimum spacing. Labels in horizontal form require <code>.control-label</code> class.</p> --}}

            <form class="form-horizontal" action="#" method="POST">
                @csrf
                <fieldset class="content-group">
                    <legend class="text-bold">Input Data Calon Penerima Bantuan</legend>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama Kelompok</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="nama_kelompok" value="{{ $p->nama_kelompok }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat</label>
                        <div class="col-lg-10">
                            <textarea rows="2" cols="5" class="form-control" name="alamat">{{ $p->alamat }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Kecamatan</label>
                        <div class="col-lg-10">
                            <select class="select-search" name="kecamatan">
                                <optgroup label="Nama Kecamatan">
                                    <option {{ ($p->kecamatan==="Denpasar Utara")?"selected":"" }} value="Denpasar Utara">Denpasar Utara</option>
                                    <option {{ ($p->kecamatan==="Denpasar Selatan")?"selected":"" }} value="Denpasar Selatan">Denpasar Selatan</option>
                                    <option {{ ($p->kecamatan==="Denpasar Barat")?"selected":"" }} value="Denpasar Barat">Denpasar Barat</option>
                                    <option {{ ($p->kecamatan==="Denpasar Timur")?"selected":"" }} value="Denpasar Timur">Denpasar Timur</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama Ketua Kelompok</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="nama_ketua_kelompok" value="{{ $p->nama_ketua_kelompok }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Status Kelompok</label>
                        <div class="col-lg-10">
                            <select class="select-search" name="status_kelompok">
                                <optgroup label="Status Kelompok">
                                    <option {{ ($p->status==="Aktif")?"selected":" " }} value="Aktif">Aktif</option>
                                    <option {{ ($p->status==="Non-Aktif")?"selected":" " }} value="Non-Aktif">Non-Aktif</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <legend class="text-bold"></legend>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Jumlah Anggota</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="jumlah_anggota" value="{{ $p->jumlah_anggota }}" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Riwayat Bantuan Pemerintah </label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="riwayat_bantuan" value="{{ $p->riwayat_bantuan }}" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Luas Lahan (m2) </label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="luas_lahan" value="{{ $p->luas_lahan }}" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Keaktifan Kelompok</label>
                        <div class="col-lg-10">
                            <select class="select" name="keaktifan_kelompok">
                                <optgroup label="Keaktifan Kelompok">
                                    <option value="">Pilih</option>
                                    <option value="1" {{ $p['keaktifan_kelompok']=='1'?'selected':'' }}>Sangat Aktif</option>
                                    <option value="2" {{ $p['keaktifan_kelompok']=='2'?'selected':'' }}>Aktif</option>
                                    <option value="3" {{ $p['keaktifan_kelompok']=='3'?'selected':'' }}>Cukup Aktif</option>
                                    <option value="4" {{ $p['keaktifan_kelompok']=='4'?'selected':'' }}>Kurang Aktif</option>
                                    <option value="5" {{ $p['keaktifan_kelompok']=='5'?'selected':'' }}>Tidak Aktif</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Lokasi Kebun Dekat Dengan Fasilitas Transportasi</label>
                        <div class="col-lg-10">
                            <select class="select" name="lokasi_kebun">
                                <optgroup label="Keaktifan Kelompok">
                                    <option value="">Pilih</option>
                                    <option value="1" {{ $p['lokasi_kebun']=='1'?'selected':'' }}>Ya</option>
                                    <option value="0" {{ $p['lokasi_kebun']=='0'?'selected':'' }}>Tidak</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    
                </fieldset>
                <div class="text-left">
                    <a href="pengajuan-bantuan" class="btn btn-primary"><i class="icon-arrow-left13 position-left"></i>Kembali </a>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>

        </div>

        
    </div>
    <!-- /basic datatable -->
    
</div>
<!-- /content area -->

@endsection