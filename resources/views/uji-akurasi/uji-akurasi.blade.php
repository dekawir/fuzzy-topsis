@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>


	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/components_modals.js')}}"></script> --}}
	<!-- /theme JS files -->


@endpush
@section('content')
<!-- Content area -->
<div class="content">

    <div class="panel-body">
        <form class="form-horizontal" action="#" method="POST">
            @csrf
            <fieldset class="content-group">
                <legend class="text-bold">Uji Akurasi</legend>
                <div class="form-group">
                    <label class="control-label col-lg-2">Persentase Data Testing</label>
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="persentase" value="{{ old('persentase') }}">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <input type="text" placeholder="%" class="form-control" disabled>
                                </div>
                            </div> 
                        </div>
                        {{-- <span class="help-block">Extensions file harus </span> --}}

                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Data Testing</label>
                    <div class="col-lg-4">
                        <select class="select" name="testing">
                            <optgroup label="Data Testing">
                                <option value="">Pilih</option>
                                <option value="Urut" {{ old('testing')=='Urut'?'selected':'' }}>Urut</option>
                                <option value="Acak" {{ old('testing')=='Acak'?'selected':'' }}>Acak</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </form>
    </div>

<hr>
    @if(!empty($fuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Nominasi Matriks</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($nominasi as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    

  
    <!-- /both borders -->


    @if(!empty($fuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Konversi Linguistik Fuzzy</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($fuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($difuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Defuzzifikasi Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($difuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif


    @if(!empty($normalisasi_matrik_fuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Normalisasi Matriks Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($normalisasi_matrik_fuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif


    @if(!empty($bobot_fuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Bobot Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($bobot_fuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif


    @if(!empty($solusi))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Solusi Ideal Positif dan Solusi Ideal Negatif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($solusi) }} --}}
                    @foreach ($solusi as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'][0] }}</td>
                        <td class="text-center">{{ $n['c1'][0] }}</td>
                        <td class="text-center">{{ $n['c2'][0] }}</td>
                        <td class="text-center">{{ $n['c3'][0] }}</td>
                        <td class="text-center">{{ $n['c4'][0] }}</td>
                        <td class="text-center">{{ $n['c5'][0] }}</td>
                     </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($alternatif_positif))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Alternatif Positif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="7" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">D+</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($alternatif_positif) }} --}}
                    @foreach ($alternatif_positif as $key=>$n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['c1'] }}</td>
                        <td class="text-center">{{ $n['c2'] }}</td>
                        <td class="text-center">{{ $n['c3'] }}</td>
                        <td class="text-center">{{ $n['c4'] }}</td>
                        <td class="text-center">{{ $n['c5'] }}</td>
                        <td class="text-center">{{ $dplus[$key]['plus'] }}</td>
                        <td class="text-center">{{ $dplus[$key]['pangkat'] }}</td>
                     </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($alternatif_negatif))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Alternatif Negatif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="7" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">D-</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($alternatif_negatif) }} --}}
                    @foreach ($alternatif_negatif as $key=>$n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['c1'] }}</td>
                        <td class="text-center">{{ $n['c2'] }}</td>
                        <td class="text-center">{{ $n['c3'] }}</td>
                        <td class="text-center">{{ $n['c4'] }}</td>
                        <td class="text-center">{{ $n['c5'] }}</td>
                        <td class="text-center">{{ $dmin[$key]['min'] }}</td>
                        <td class="text-center">{{ $dmin[$key]['pangkat'] }}</td>
                     </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($dplus))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Jarak Solusi Ideal Positif dan Solusi Ideal Negatif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th colspan="2" class="text-center">D +</th>
                            </tr>
                      
                        </thead>
                        <tbody>
                            {{-- {{ dd($alternatif_negatif) }} --}}
                            @foreach ($dplus as $key=>$n)
                            <tr>
                                <td class="text-center">{{ $dplus[$key]['a'] }}</td>
                                <td class="text-center">{{ $dplus[$key]['pangkat'] }}</td>
                             </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th colspan="2" class="text-center">D -</th>
                            </tr>
                      
                        </thead>
                        <tbody>
                            {{-- {{ dd($alternatif_negatif) }} --}}
                            @foreach ($dmin as $key=>$n)
                            <tr>
                                <td class="text-center">{{ $dmin[$key]['a'] }}</td>
                                <td class="text-center">{{ $dmin[$key]['pangkat'] }}</td>
                             </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <!-- /both borders -->
    @endif


    @if(!empty($rank))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Hasil Perangkingan Untuk Semua Alternatif Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th rowspan="" class="text-center">Vi</th>
                                <th colspan="" class="text-center">Nilai</th>
                                <th colspan="" class="text-center">Rank</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rank as $key=> $n)
                            <tr>
                                <td class="text-center">{{ $n['v'] }}</td>
                                <td class="text-center">{{ $n['vi'] }}</td>
                                <td class="text-center">{{ $n['rank'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th rowspan="" class="text-center">Vi</th>
                                <th colspan="" class="text-center">Nilai</th>
                                <th colspan="" class="text-center">Data Training</th>
                                <th colspan="" class="text-center">Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($hasil as $key=> $n)
                            <tr>
                                <td class="text-center">{{ $n['v'] }}</td>
                                <td class="text-center">{{ $n['vi'] }}</td>
                                <td class="text-center">{{ $n['training'] }}</td>
                                <td class="text-center">{{ $n['grade'] }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /both borders -->
    @endif

</div>
<!-- /content area -->

@if(!empty($alternatif_negatif))
<!-- Both borders -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Confusion Matrik</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered" >
            <thead>
                <tr class="bg-blue">
                    <th rowspan="" class="text-center"></th>
                    <th colspan="2" class="text-center">Actual</th>
                    <th rowspan="4" class="text-center">Total</th>
                </tr>
                <tr>
                    <th class="text-center bg-blue">Prediksi</th>
                    <th class="text-center">Lolos</th>
                    <th class="text-center">Tidak Lolos</th>
                </tr>
                <tr>
                    <th class="text-center bg-blue">Lolos</th>
                    <th class="text-center">{{ $conf['TP'] }}</th>
                    <th class="text-center">{{ $conf['FP'] }}</th>
                    
                </tr>
                <tr>
                    <th class="text-center bg-blue">Tidak Lolos</th>
                    <th class="text-center">{{ $conf['FN'] }}</th>
                    <th class="text-center">{{ $conf['TN'] }}</th>
                    
                </tr>
                <tr>
                    <th  class="text-center"></th>
                    <th class="text-center">{{$conf['TP'] + $conf['FN'] }}</th>
                    <th class="text-center">{{$conf['FP'] + $conf['TN'] }}</th>
                    <th class="text-center">{{$conf['FP'] + $conf['TN'] + $conf['TP'] + $conf['FN'] }}</th>

                   
                </tr>
            </thead>
            
        </table>
    </div>
    <br><br><br>
    <div class="table-responsive">
        <table class="table table-bordered" >
            <thead>
                <tr>
                    <th  class="text-center bg-blue">Akurasi</th>
                    <th  class="text-center">{{ $conf['TP'] + $conf['TN'] / ($conf['FP'] + $conf['TN'] + $conf['TP'] + $conf['FN'])  }}</th>
                </tr>
                <tr>
                    <th  class="text-center bg-blue">Pesisi</th>
                    <th  class="text-center">{{ $conf['TP'] / ($conf['TP'] + $conf['FP']) }}</th>
                </tr>
                <tr>
                    <th  class="text-center bg-blue">Recall</th>
                    <th  class="text-center">{{ $conf['TP']/ ($conf['TP']+$conf['FN']) }}</th>
                </tr>
                
            </thead>
        </table>
    </div>
</div>
<!-- /both borders -->
@endif
    

<!-- /primary modal -->
@endsection