@extends('layouts.index')
@push('asset')

    <script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/dashboard.js')}}"></script>

@endpush
@section('content')
<!-- Dashboard content -->
<div class="row">
    <div class="media">
        <a href="#" class="media-centre"><img src="{{ asset('logo.png') }}" class="" style="margin-left: auto; margin-right: auto; width: 20%; display: block" alt=""></a>
    </div>
    <div class="col-lg-12 mt-20">
            <h1 class="no-margin text-black" style="text-align: center">
                SISTEM PENDUKUNG KEPUTUSAN PEMBERIAN
                BANTUAN PEKARANGAN PANGAN LESTARI
                (P2L) DENGAN MENGGUNAKAN METODE
                FUZZY TOPSIS PADA DINAS PERIKANAN DAN
                KETAHANAN PANGAN KOTA DENPASAR</h1>
    </div>

    {{-- <div class="col-lg-8 mt-10">

        <!-- Quick stats boxes -->
        <div class="row">
            <div class="col-lg-4">

                <!-- Members online -->
                <div class="panel bg-teal-400">
                    <div class="panel-body">
                        <h3 class="no-margin">d</h3>
                        Category
                        <div class="text-muted text-size-small"> Active</div>
                    </div>

                    <div class="container-fluid">
                        <div id="members-online"></div>
                    </div>
                </div>
                <!-- /members online -->

            </div>

            <div class="col-lg-4">

                <!-- Current server load -->
                <div class="panel bg-pink-400">
                    <div class="panel-body">
                        <h3 class="no-margin"></h3>
                        Carving
                        <div class="text-muted text-size-small"> Active</div>
                    </div>

                    <div id="server-load"></div>
                </div>
                <!-- /current server load -->

            </div>

            <div class="col-lg-4">

                <!-- Today's revenue -->
                <div class="panel bg-blue-400">
                    <div class="panel-body">
                        <h3 class="no-margin"></h3>
                        Gallery
                        <div class="text-muted text-size-small">Active</div>
                    </div>

                    <div id="today-revenue"></div>
                </div>
                <!-- /today's revenue -->

            </div>
        </div>
        <!-- /quick stats boxes -->

    </div> --}}

</div>
<!-- /dashboard content -->
@endsection