@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>


	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

	
	<!-- /theme JS files -->

<script>


// Pie chart

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawPie);


// Chart settings    
function drawPie() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Task', 'Hours per Day'],
        ['Lolos',     {{ $lolos }}],
        ['Tidak Lolos',      {{ $tidaklolos }}],

    ]);

    // Options
    var options_pie = {
        fontName: 'Roboto',
        height: 300,
        width: 500,
        chartArea: {
            left: 50,
            width: '90%',
            height: '90%'
        }
    };
    

    // Instantiate and draw our chart, passing in some options.
    var pie = new google.visualization.PieChart($('#google-pie')[0]);
    pie.draw(data, options_pie);
}
</script>


@endpush
@section('content')
<!-- Content area -->
<div class="content">
    @if(!empty($hasil))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Nilai Preferensi Untuk Setiap Alternatif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="" class="text-center">Vi</th>
                        <th colspan="" class="text-center">Nilai</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($hasil as $n)
                    <tr>
                        <td class="text-center">{{ $n['v'] }}</td>
                        <td class="text-center">{{ $n['vi'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    <!-- Both borders -->
    @if(!empty($rank))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Hasil Perangkingan Untuk Semua Alternatif Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th rowspan="" class="text-center">Vi</th>
                                <th colspan="" class="text-center">Nilai</th>
                                <th colspan="" class="text-center">Rank</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rank as $key=> $n)
                            <tr>
                                <td class="text-center">{{ $n['v'] }}</td>
                                <td class="text-center">{{ $n['vi'] }}</td>
                                <td class="text-center">{{ $n['rank'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th rowspan="" class="text-center">Vi</th>
                                <th colspan="" class="text-center">Nilai</th>
                                <th colspan="" class="text-center">Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($hasil as $key=> $n)
                            <tr>
                                <td class="text-center">{{ $n['v'] }}</td>
                                <td class="text-center">{{ $n['vi'] }}</td>
                                <td class="text-center">
                                    @if($n['vi']>0.50 && $n['vi'] <=1)  <span class="bg-success">Lolos</span>
                                    @else <span class="bg-danger">Tidak Lolos</span>
                                    @endif
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /both borders -->
    @endif
    <!-- Both borders -->

  {{-- <div style="margin-bottom: 20px;">
      <form action="" method="POST">
          @csrf
          <button type="submit" class="btn btn-primary">Hitung <i class="icon-stack-check"></i></button>
      </form>
  </div> --}}

    <!-- Basic pie charts -->
    <div class="row">
        <div class="">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Basic pie chart</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="chart-container text-center">
                        <div class="display-inline-block" id="google-pie"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /basic pie charts -->

</div>
<!-- /content area -->

    

<!-- /primary modal -->
@endsection