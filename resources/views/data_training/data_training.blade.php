@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_sorting.js')}}"></script>
	

    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>


	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput.min.js')}}"></script>
	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js')}}"></script> --}}
	<!-- /theme JS files -->
	<!-- /theme JS files -->

    <script>

        $(function() {

        // Basic example
        $('.file-input').fileinput({
            browseLabel: '',
            browseClass: 'btn btn-primary btn-icon',
            removeLabel: '',
            uploadLabel: '',
            uploadClass: 'btn btn-default btn-icon',
            browseIcon: '<i class="icon-plus22"></i> ',
            uploadIcon: '<i class="icon-file-upload"></i> ',
            removeClass: 'btn btn-danger btn-icon',
            removeIcon: '<i class="icon-cancel-square"></i> ',
            layoutTemplates: {
                caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
            },
            initialCaption: "No file selected"
        });


        // With preview
        $(".file-input-preview").fileinput({
            browseLabel: '',
            browseClass: 'btn btn-primary btn-icon',
            removeLabel: '',
            uploadLabel: '',
            uploadClass: 'btn btn-default btn-icon',
            browseIcon: '<i class="icon-plus22"></i> ',
            uploadIcon: '<i class="icon-file-upload"></i> ',
            removeClass: 'btn btn-danger btn-icon',
            removeIcon: '<i class="icon-cancel-square"></i> ',
            layoutTemplates: {
                caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
            },
            initialPreview: [
                "<img src='assets/images/placeholder.jpg' class='file-preview-image' alt=''>",
                "<img src='assets/images/placeholder.jpg' class='file-preview-image' alt=''>",
            ],
            overwriteInitial: false,
            maxFileSize: 100
        });


        // Display preview on load
        $(".file-input-overwrite").fileinput({
            browseLabel: '',
            browseClass: 'btn btn-primary btn-icon',
            removeLabel: '',
            uploadLabel: '',
            uploadClass: 'btn btn-default btn-icon',
            browseIcon: '<i class="icon-plus22"></i> ',
            uploadIcon: '<i class="icon-file-upload"></i> ',
            removeClass: 'btn btn-danger btn-icon',
            removeIcon: '<i class="icon-cancel-square"></i> ',
            layoutTemplates: {
                caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
            },
            initialPreview: [
                "<img src='assets/images/placeholder.jpg' class='file-preview-image' alt=''>",
                "<img src='assets/images/placeholder.jpg' class='file-preview-image' alt=''>",
            ],
            overwriteInitial: true
        });


        // Custom layout
        $('.file-input-custom').fileinput({
            previewFileType: 'image',
            browseLabel: 'Select',
            browseClass: 'btn bg-slate-700',
            browseIcon: '<i class="icon-image2 position-left"></i> ',
            removeLabel: 'Remove',
            removeClass: 'btn btn-danger',
            removeIcon: '<i class="icon-cancel-square position-left"></i> ',
            uploadClass: 'btn bg-teal-400',
            uploadIcon: '<i class="icon-file-upload position-left"></i> ',
            layoutTemplates: {
                caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
            },
            initialCaption: "No file selected"
        });


        // Advanced example
        $('.file-input-advanced').fileinput({
            browseLabel: 'Browse',
            browseClass: 'btn btn-default',
            removeLabel: '',
            uploadLabel: '',
            browseIcon: '<i class="icon-plus22 position-left"></i> ',
            uploadClass: 'btn btn-primary btn-icon',
            uploadIcon: '<i class="icon-file-upload"></i> ',
            removeClass: 'btn btn-danger btn-icon',
            removeIcon: '<i class="icon-cancel-square"></i> ',
            initialCaption: "No file selected",
            layoutTemplates: {
                caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>',
                main1: "{preview}\n" +
                "<div class='input-group {class}'>\n" +
                "   <div class='input-group-btn'>\n" +
                "       {browse}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "   <div class='input-group-btn'>\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "</div>"
            }
        });


        // Disable/enable button
        $("#btn-modify").on("click", function() {
            $btn = $(this);
            if ($btn.text() == "Disable file input") {
                $("#file-input-methods").fileinput("disable");
                $btn.html("Enable file input");
                alert("Hurray! I have disabled the input and hidden the upload button.");
            }
            else {
                $("#file-input-methods").fileinput("enable");
                $btn.html("Disable file input");
                alert("Hurray! I have reverted back the input to enabled with the upload button.");
            }
        });


        // Custom file extensions
        $(".file-input-extensions").fileinput({
            browseLabel: 'Browse',
            browseClass: 'btn btn-primary',
            removeLabel: '',
            browseIcon: '<i class="icon-plus22 position-left"></i> ',
            uploadIcon: '<i class="icon-file-upload position-left"></i> ',
            removeClass: 'btn btn-danger btn-icon',
            removeIcon: '<i class="icon-cancel-square"></i> ',
            layoutTemplates: {
                caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
            },
            initialCaption: "No file selected",
            maxFilesNum: 10,
            allowedFileExtensions: ["csv", "xls", "xlsx"]
        });

        });

        
    </script>


@endpush
@section('content')
<!-- Content area -->
<div class="content">

    <!-- Form horizontal -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            {{-- <h5 class="panel-title">Basic form inputs</h5> --}}
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- <p class="content-group-lg">Examples of standard form controls supported in an example form layout. Individual form controls automatically receive some global styling. All textual <code>&lt;input></code>, <code>&lt;textarea></code>, and <code>&lt;select></code> elements with <code>.form-control</code> are set to <code>width: 100%;</code> by default. Wrap labels and controls in <code>.form-group</code> for optimum spacing. Labels in horizontal form require <code>.control-label</code> class.</p> --}}

            <form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="content-group">
                    <legend class="text-bold">Data Training</legend>
                    <div class="form-group">
                        <label class="col-lg-2 control-label text-semibold">Upload File</label>
                        <div class="col-lg-10">
                            <input type="file" name="file" class="file-input-extensions">
                            <span class="help-block">Extensions file harus <code>csv</code>, <code>xls</code>, <code>xlsx</code></span>
                        </div>
                    </div>
                </fieldset>
            </form>

            <legend class="text-bold">Data Training</legend>
            <form action="" method="POST">
                @csrf
                <button type="submit" name="kosongkan" value="kosong" class="btn btn-primary">Kosongkan Data Training <i class="icon-arrow-right14 position-right"></i></button>
                
            </form>
            <table class="table datatable-sorting">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kelompok</th>
                        <th>Nama Ketua</th>
                        <th>Tanggal Pengajuan</th>
                        <th>Keaktifan Kelompok</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $p)    
                    <tr>
                        <td>A {{ $key+1 }}</td>
                        <td>{{ $p->nama_kelompok }}</td>
                        <td>{{ $p->nama_ketua_kelompok }}</td>
                        <td>{{ date('d/m/Y', strtotime($p->tanggal_pengajuan)) }}</td>
                        <td>
                            @if($p->keaktifan_kelompok=='1')<span class="label label-success">Sangat Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='2') <span class="label label-primary"> Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='3') <span class="label label-default">Cukup Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='4') <span class="label label-warning">Kurang Aktif</span> 
                            @elseif($p->keaktifan_kelompok=='5') <span class="label label-danger">Tidak Aktif</span> 
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="pengajuan-bantuan-ubah?pengajuan={{ $p->id }}" type="button" class="btn btn-info btn-xs"><i class="icon-pencil7"></i></a>
                            <button type="button" onclick="del({{ $p->id }})" data-category="{{ $p->id }}"" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal_theme_primary"> <i class="icon-trash"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /form horizontal -->
</div>
<!-- /content area -->

<!-- Primary modal -->
<div id="modal_theme_primary" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('del.pengajuan') }}" method="POST">
                @csrf
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Konfirmasi</h6>
                </div>

                <div class="modal-body">
                    <h6 class="text-semibold">Apakah anda yakin menghapus data ini?</h6>
                    <input type="hidden" id="delUser" name="id">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                    <button type="submit"  class="btn btn-primary">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /primary modal -->

{{-- Skrip tarik data modal confirmation --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delUser");
        input.value = id;
        // console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>
@endsection