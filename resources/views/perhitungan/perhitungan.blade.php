@extends('layouts.index')
@push('asset')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    <!-- /theme JS files -->

    <!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js')}}"></script>    
	<!-- /theme JS files -->
    
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/datepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>


	<script type="text/javascript" src="{{ asset('assets/js/pages/picker_date.js')}}"></script>
	{{-- <script type="text/javascript" src="{{ asset('assets/js/pages/components_modals.js')}}"></script> --}}
	<!-- /theme JS files -->


@endpush
@section('content')
<!-- Content area -->
<div class="content">
    
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Nominasi Matriks</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($nominasi as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->

    <!-- Both borders -->
  <div style="margin-bottom: 20px;">
      <form action="" method="POST">
          @csrf
          <button type="submit" class="btn btn-primary">Hitung <i class="icon-stack-check"></i></button>
      </form>
  </div>

  
    <!-- /both borders -->


    @if(!empty($fuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Konversi Linguistik Fuzzy</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($fuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($difuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Defuzzifikasi Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($difuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif


    @if(!empty($normalisasi_matrik_fuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Normalisasi Matriks Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($normalisasi_matrik_fuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif


    @if(!empty($bobot_fuzzy))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Bobot Fuzzy Topsis</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($bobot_fuzzy as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['jumlah_anggota'] }}</td>
                        <td class="text-center">{{ $n['riwayat_bantuan'] }}</td>
                        <td class="text-center">{{ $n['luas_lahan'] }}</td>
                        <td class="text-center">{{ $n['keaktifan_kelompok'] }}</td>
                        <td class="text-center">{{ $n['lokasi_kebun'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif


    @if(!empty($solusi))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Solusi Ideal Positif dan Solusi Ideal Negatif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="5" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($solusi) }} --}}
                    @foreach ($solusi as $n)
                    <tr>
                        <td class="text-center">{{ $n['a'][0] }}</td>
                        <td class="text-center">{{ $n['c1'][0] }}</td>
                        <td class="text-center">{{ $n['c2'][0] }}</td>
                        <td class="text-center">{{ $n['c3'][0] }}</td>
                        <td class="text-center">{{ $n['c4'][0] }}</td>
                        <td class="text-center">{{ $n['c5'][0] }}</td>
                     </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($alternatif_positif))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Alternatif Positif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="7" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">D+</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($alternatif_positif) }} --}}
                    @foreach ($alternatif_positif as $key=>$n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['c1'] }}</td>
                        <td class="text-center">{{ $n['c2'] }}</td>
                        <td class="text-center">{{ $n['c3'] }}</td>
                        <td class="text-center">{{ $n['c4'] }}</td>
                        <td class="text-center">{{ $n['c5'] }}</td>
                        <td class="text-center">{{ $dplus[$key]['plus'] }}</td>
                        <td class="text-center">{{ $dplus[$key]['pangkat'] }}</td>
                     </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($alternatif_negatif))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Alternatif Negatif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr class="bg-blue">
                        <th rowspan="2" class="text-center">Alternatif</th>
                        <th colspan="7" class="text-center">Kategori</th>
                    </tr>
                    <tr class="bg-blue">
                        <th class="text-center">C1</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">D-</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($alternatif_negatif) }} --}}
                    @foreach ($alternatif_negatif as $key=>$n)
                    <tr>
                        <td class="text-center">{{ $n['a'] }}</td>
                        <td class="text-center">{{ $n['c1'] }}</td>
                        <td class="text-center">{{ $n['c2'] }}</td>
                        <td class="text-center">{{ $n['c3'] }}</td>
                        <td class="text-center">{{ $n['c4'] }}</td>
                        <td class="text-center">{{ $n['c5'] }}</td>
                        <td class="text-center">{{ $dmin[$key]['min'] }}</td>
                        <td class="text-center">{{ $dmin[$key]['pangkat'] }}</td>
                     </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /both borders -->
    @endif

    @if(!empty($dplus))
    <!-- Both borders -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Jarak Solusi Ideal Positif dan Solusi Ideal Negatif</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {{-- Example of a fully <code>bordered</code> table. Here we have both vertical and horizontal borders displayed. All borders have the same color, table <code>head</code> is visually divided from the table <code>body</code> with a bit darker border color. To use this layout add <code>.table-bordered</code> class to the table with <code>.table</code> class. --}}
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th colspan="2" class="text-center">D +</th>
                            </tr>
                      
                        </thead>
                        <tbody>
                            {{-- {{ dd($alternatif_negatif) }} --}}
                            @foreach ($dplus as $key=>$n)
                            <tr>
                                <td class="text-center">{{ $dplus[$key]['a'] }}</td>
                                <td class="text-center">{{ $dplus[$key]['pangkat'] }}</td>
                             </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="bg-blue">
                                <th colspan="2" class="text-center">D -</th>
                            </tr>
                      
                        </thead>
                        <tbody>
                            {{-- {{ dd($alternatif_negatif) }} --}}
                            @foreach ($dmin as $key=>$n)
                            <tr>
                                <td class="text-center">{{ $dmin[$key]['a'] }}</td>
                                <td class="text-center">{{ $dmin[$key]['pangkat'] }}</td>
                             </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <!-- /both borders -->
    @endif

</div>
<!-- /content area -->

    

<!-- /primary modal -->
@endsection